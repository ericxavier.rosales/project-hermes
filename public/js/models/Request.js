function Request(req) {
    this.RequestId      = ko.observable(req.request_id)
    this.BeneficiaryId  = ko.observable(req.beneficiary_id)
    this.Name           = ko.observable(req.name)
    this.TrackingNumber = ko.observable(req.tracking_number)
    this.ContactPerson  = ko.observable(req.contact_person)
    this.ContactNumber  = ko.observable(req.contact_number)

    this.Type           = ko.observable((req.type != null) ? req.type : '' )
    this.Classification = ko.observable((req.classification != null) ? req.classification : '')
    this.RequestType    = ko.observable((req.request_type != null) ? req.request_type : '')
    this.Designation    = ko.observable((req.designation != null) ? req.designation : '')
    this.Receiver       = ko.observable((req.receiver != null) ? req.receiver : '')
    this.Allocation     = ko.observable((req.allocation != null) ? req.allocation : 0)
    this.Requests       = ko.observable((req.requests != null) ? req.requests : '') 
    this.Remarks        = ko.observable((req.remarks != null) ? req.remarks : '')
    this.Tag            = ko.observable(req.tag)

    this.CityId         = ko.observable(req.city_id)
    this.CityName       = ko.observable(req.city_name)
    this.ProvinceId     = ko.observable(req.province_id)
    this.ProvinceName   = ko.observable(req.province_name)
    this.RegionId       = ko.observable(req.region_id)
    this.RegionName     = ko.observable(req.region_name)
    this.Location       = ko.observable(req.location)

    this.IsChecked      = ko.observable(false)
    this.DeliveryDate   = ko.observable((req.date_delivered != null) ? moment(req.date_delivered).utcOffset(960).format('YYYY-MM-DD') : '')
    this.RequestDate    = ko.observable(req.request_date)
    this.CreatedAt      = ko.observable(req.created_at)

    this.getTagName = () => {
        let tagName = ''
        switch (this.Tag()) {
            case 0:
                tagName = 'New'
                break
            case 1:
                tagName = 'Confirmed'
                break
            case 2:
                tagName = 'Approved'
                break
            case 3:
                tagName = 'Fulfilled'
                break
            case 4:
                tagName = 'In Transit'
                break
            case 5:
                tagName = 'Delivered'
                break
            default:
                tagName = 'New'
                break
        }

        return tagName
    }

    this.getRequestTypeName = () => {
        let requestTypeName = ''
        switch (this.RequestType()) {
            case 'P':
                requestTypeName = 'PPE(s)'
                break
            case 'T':
                requestTypeName = 'Medical Supplies'
                break
            default:
                requestTypeName = ''
                break
        }
        
        return requestTypeName
    }

}

// module.exports = Request