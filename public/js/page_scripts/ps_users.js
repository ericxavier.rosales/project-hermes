$(document).ready(() => {
    console.log('Users page loaded!')

    function UsersViewModel() {
        let self = this
        // Base View Model
        allRegions	 = []
        allProvinces	 = []
        allCities	 = []
        allTotalRequests = []
        ko.BaseViewModel.call(self)
        // Selectors
        const changePassForm        = document.querySelector('#password-change-form')
        const newUserForm           = document.querySelector('#create-user-form')

        // Observables
        self.isSuperUser            = ko.observable(role == 'SU')
        self.isCallCenter           = ko.observable(role == 'CC')

        self.changeOldPass          = ko.observable('')
        self.changeNewPass          = ko.observable('')
        self.changeNewPassConfirm   = ko.observable('')

        self.newUsername            = ko.observable('')
        self.newUserPassword        = ko.observable('')
        self.newUserConfirmPassword = ko.observable('')
        self.newUserRole            = ko.observable('')

        // Listeners
        changePassForm.addEventListener('submit', e => {
            e.preventDefault()
            if (self.validatePasswordMatch('change-pass') && confirm('Are you sure you want to change your password?')) {
                self.changePassword()
            } else if (!self.validatePasswordMatch('change-pass')) {
                M.toast({
                    html: 'Passwords do not match!',
                    classes: 'yellow darken-4',
                    displayLength: 2500
                })
            }
        })

        newUserForm.addEventListener('submit', e => {
            e.preventDefault()
            if (self.validatePasswordMatch('create-user') && confirm('Are you sure you want to create this user?')) {
                self.createUser()
            } else if (!self.validatePasswordMatch('create-user')) {
                M.toast({
                    html: 'Passwords do not match!',
                    classes: 'yellow darken-4',
                    displayLength: 2500
                })
            }
        })

        // Functions 
        self.changePassword = () => {
            $.ajax({
                method: 'POST',
                url: '/login/change-password',
                data: {
                    oldPass: self.changeOldPass(),
                    newPass: self.changeNewPass()
                }
            }).done(res => {
                if (res) {
                    M.toast({
                        html: 'Password change success, logging you out...',
                        classes: 'teal',
                        displayLength: 1500
                    })
                    setTimeout(() => {
                        window.location.href = '/logout'
                    }, 2000)
                } else {
                    M.toast({
                        html: 'Old password is incorrect!',
                        classes: 'red',
                        displayLength: 2500
                    })
                }
            })
        }

        self.createUser = () => {
            $.ajax({
                method: 'POST',
                url: '/login/create-user',
                data: {
                    user: self.newUsername(),
                    pass: self.newUserPassword(),
                    role: self.newUserRole()
                }
            }).done(res => {
                let createResult = res.createResult

                if (createResult == -1) {
                    M.toast({
                        html: 'User already exists in database!',
                        classes: 'yellow darken-4',
                        displayLength: 2500
                    })
                } else if (createResult == 0) {
                    M.toast({
                        html: 'Error encountered while creating user!',
                        classes: 'red',
                        displayLength: 2500
                    })
                } else {
                    M.toast({
                        html: 'New User created!',
                        classes: 'teal',
                        displayLength: 2500
                    })

                    self.clearCreateUserForm()
                }
            })
        }


        // Validations 
        self.validatePasswordMatch = (form) => {
            if (form == 'change-pass') {
                return self.changeNewPass() == self.changeNewPassConfirm()
            } else {
                return self.newUserPassword() == self.newUserConfirmPassword()
            }
        }


        // Misc 
        self.clearCreateUserForm = () => {
            self.newUsername('')
            self.newUserPassword('')
            self.newUserConfirmPassword('')
            self.newUserRole('')

            M.updateTextFields()
            $('#new-userrole').formSelect()
        }

        if (firstLogin) {
            $('.tap-target').tapTarget()
            $('#first-login-highlight').tapTarget('open')
            setTimeout(() => {
                $('#first-login-highlight').tapTarget('close')
            }, 2500)
        }
    }

    ko.applyBindings(new UsersViewModel())
})
