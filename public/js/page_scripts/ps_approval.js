$(document).ready(() => {
    console.log('Approval page loaded!')

    function ApprovalViewModel() {
        let self = this
        // Base View Model
        ko.BaseViewModel.call(self)
        // Variables 

        // Arrays 

        // Initialization 
        self.tagFilter(1)
        $('#filter-sidenav-tag').attr('disabled', true)

        // Computed 
        self.filterDetails      = ko.pureComputed(() => {
            let searchBarFilter = (self.searchBar() != '')
                ? `With Name(s)/Tracking Number(s) containing '${self.searchBar()}' \n`
                : ''

            let dateFilter = ''
            if (self.dateFilterOp() == 'eq' && self.dateFrom() != '') {
                dateFilter = `With Date(s) equal to ${moment(self.dateFrom()).format('MMMM D, YYYY')} \n`
            } else if (self.dateFilterOp() == 'lt' && self.dateTo() != '') {
                dateFilter = `With Date(s) on or before ${moment(self.dateTo()).format('MMMM D, YYYY')} \n`
            } else if (self.dateFilterOp() == 'gt' && self.dateFrom() != '') {
                dateFilter = `With Date(s) on or after ${moment(self.dateFrom()).format('MMMM D, YYYY')} \n`
            } else if (self.dateFilterOp() == 'bw' && self.dateTo() != '' && self.dateFrom() != '') {
                dateFilter = `With Date(s) on or between ${moment(self.dateFrom()).format('MMMM D, YYYY')} and ${moment(self.dateTo()).format('MMMM D, YYYY')} \n`
            } else {
                dateFilter = ''
            }

            let regionFilter = ''
            if (self.region() != '' && self.region() != null) {
                let region = ko.utils.arrayFirst(allRegions, reg => {
                    return reg.id == self.region()
                })
                regionFilter = `Region: ${region.name}`
            }

            let provinceFilter = ''
            if (self.province() != '' && self.province() != null) {
                let province = ko.utils.arrayFirst(self.filteredProvinces(), province => {
                    return province.id == self.province()
                })
                provinceFilter = `Province: ${province.name}`
            }

            let cityFilter = '' 
            if (self.city() != '' && self.city() != null) {
                let city = ko.utils.arrayFirst(self.filteredCities(), city => {
                    return city.id == self.city()
                })
                cityFilter = `City: ${city.name}`
            }


            let locationFilter = (regionFilter != '' || provinceFilter != '' || cityFilter != '')
                ? `With location in ${regionFilter} ${provinceFilter} ${cityFilter}`
                : ''

            return searchBarFilter + dateFilter + locationFilter
        })
        
        // Listeners
		$('body').on('click', '#toast-delete', e => {
            M.Toast.dismissAll()
            self.selectedRequests([ self.selectedRequest().RequestId() ])
            self.deleteSelectedRequests()
            
            $('#details-beneficiary-type').formSelect()
            $('#details-request-type').formSelect()
            $('#request-details-modal').modal('close')
        })

        // Methods 
        self.approveRequest = async () => {
			let name = self.selectedRequest().Name()
			let city = self.selectedRequest().CityId()

			let validationResult = await self.validateDuplicates(name, city)
			let duplicatePendingRequests = ko.utils.arrayFilter(validationResult.rows, row => {
				return row.id != self.selectedRequest().BeneficiaryId() && row.tag < 5
			})

			let hasPending = duplicatePendingRequests.length > 0

            if (hasPending) {
				M.toast({
					html: `
						<p><strong>WARNING:</strong> That institution already has a pending request that has not been delivered.<br>You may choose to delete this request.</p>
						<a id='toast-delete' class='btn-flat toast-action'>DELETE</a>
					`, 
					classes: 'red lighten-1',
					displayLength: 5500
				})
			} else if (self.validateDetailForm() && confirm(`Approve the selected request?`)) {
                await self.saveChanges()
                
                $.ajax({
                    method: 'POST',
                    url: '/approval',
                    data: {
                        requestIds: [ self.selectedRequest().RequestId() ]
                    }
                }).done(res => {
                    if (res) {
                        $('#request-details-modal').modal('close')

                        self.allRequests.remove(request => {
                            return self.selectedRequest().RequestId() == request.RequestId()
                        })

                        self.selectedRequest(new Request({}))
                        M.toast({ html: 'Request(s) approved!', classes: 'teal' })
                    }
                })
            } else if (!self.validateDetailForm()) {
                M.toast({
                    html: 'All fields are required!',
                    classes: 'yellow darken-3',
                    displayLength: 1750
                })
            }
        }

        self.approveSelectedRequests = () => {
            let selectedCount = self.selectedRequests().length
            if (selectedCount && confirm('Approve the selected request(s)?')) {
                $.ajax({
                    method: 'POST',
                    url: '/approval',
                    data: {
                        requestIds: self.selectedRequests()
                    }
                }).done(res => {
                    if(res) {

                        self.allRequests.remove(request => {
                            return self.selectedRequests.indexOf(request.RequestId()) > -1
                        })

                        self.selectedRequests([])
                        M.toast({ html: 'Request(s) approved!', classes: 'teal' })
                    }
                })
            } else if (selectedCount == 0) {
                M.toast({ 
                    html: 'No requests selected!', 
                    classes: 'yellow darken-3', 
                    displayLength: 2000 
                })
            }
		}
		
		self.confirmSaveChanges = async () => {
			let name = self.selectedRequest().Name()
			let city = self.selectedRequest().CityId()

			let validationResult = await self.validateDuplicates(name, city)

			let duplicatePendingRequests = ko.utils.arrayFilter(validationResult.rows, row => {
				return row.id != self.selectedRequest().BeneficiaryId() && row.tag < 5
			})

			let hasPending = duplicatePendingRequests.length > 0

			if (hasPending) {
				M.toast({
					html: `
						<p><strong>WARNING:</strong> That institution already has a pending request that has not been delivered.<br>You may choose to delete this request.</p>
						<a id='toast-delete' class='btn-flat toast-action'>DELETE</a>
					`, 
					classes: 'red lighten-1',
					displayLength: 5500
				})
			} else if (confirm("Save these changes?")) {
				let result = await self.saveChanges()
				if (result) {
					self.selectedRequest(new Request({}))
	
					$('#request-details-modal').modal('close')
					M.toast({ html: 'Request updated successfully!', classes: 'teal'})  
				} 
			}

        }

		// Validation

        // Misc
        self.exportRequestsAsXls = () => {
            let tempRequests = []
            let exportDate = [{ 
                A : 'Export Date:', 
                B : moment().format('MM-DD-YYYY')
            }]
            let exportFilter = [{
                A : 'Export Filter:',
                B : (self.filterDetails() != '') ? self.filterDetails() : 'NONE'
            }]
            let headers = [{ 
                A : 'Tracking Number', 
                B : 'Request Date', 
                C : 'Contact Person', 
                D : 'Designation', 
                E : 'City', 
                F : 'Province', 
                G : 'Region', 
                H : 'Beneficiary Name',
                I : 'Type', 
                J : 'Classification',
                K : 'Contact Person', 
                L : 'Request Type', 
                M : 'Requests', 
                N : 'Allocation', 
                O : 'Remarks', 
                P : 'Receiver' 
            }]
            let merge = [
                { 
                    s : { r : 1, c : 0 }, 
                    e : { r : 2, c : 0 }
                },
                {
                    s : { r : 1, c : 1 },
                    e : { r : 2, c : 15 }
                }
            ]

            ko.utils.arrayForEach(self.filteredRequests(), req => {
                tempRequests.push({
                    TrackingNumber: req.TrackingNumber(),
                    RequestDate: moment(req.CreatedAt()).utcOffset(960).format('MM-DD-YYYY'),
                    ContactPerson: req.ContactPerson(),
                    Designation: req.Designation(),
                    City: req.CityName(),
                    Province: req.ProvinceName(),
                    Region: req.RegionName(),
                    BeneficiaryName: req.Name(),
                    Type: req.Type(),
                    Classification: req.Classification(),
                    ContactNumber: `'${req.ContactNumber()}`,
                    RequestType: req.RequestType(),
                    Requests: req.Requests(),
                    Allocation: req.Allocation(),
                    Remarks: req.Remarks(),
                    Receiver: req.Receiver()
                })
            })

            let filename = `Approval-Export-${generateTimestamp()}`
            let wb = XLSX.utils.book_new()
            let ws = XLSX.utils.json_to_sheet(exportDate, { skipHeader: true })
            XLSX.utils.sheet_add_json(ws, exportFilter, { origin: 'A2', skipHeader: true })
            XLSX.utils.sheet_add_json(ws, headers, { origin: 'A5', skipHeader: true })
            XLSX.utils.sheet_add_json(ws, tempRequests, { origin: 'A6', skipHeader: true })
            ws['!merges'] = merge
            XLSX.utils.book_append_sheet(wb, ws)
            XLSX.writeFile(wb, `${filename}.xls`)

            M.toast({
                html: `Successfully exported ${filename}!`,
                classes: 'teal'
            })
            tempRequests = null
        }
    }

    ko.applyBindings(new ApprovalViewModel())
})