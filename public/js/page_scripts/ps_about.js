$(document).ready(() => {
    console.log('About page loaded!')

    function AboutViewModel() {
        let self = this
        // Base View Model
        allRegions 	 = []
        allProvinces  	 = []
        allCities 	 = []
        allTotalRequests = []
        ko.BaseViewModel.call(self)

        self.isSuperUser    = ko.observable(role == 'SU')
        self.isCallCenter   = ko.observable(role == 'CC')
    }

    ko.applyBindings(new AboutViewModel())
})
