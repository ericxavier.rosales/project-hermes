$(document).ready(() => {
    console.log('Fulfillment page loaded!')

    function FulfillmentViewModel() {
        let self = this
        // Base View Model
        ko.BaseViewModel.call(self)
        // Variables 
        self.isDownloading      = ko.observable(false)

        // Arrays 
        self.allocations        = ko.observableArray([])

        // Initialization 
        self.tagFilter(2)
        $('#filter-sidenav-tag').attr('disabled', true)
        $('#filter-sidenav-tag').formSelect()

        // Computed 

        // Listeners

        // Methods 
        self.fulfillRequest = async () => {
            let isAllocationValid = (self.selectedRequest().RequestType() == 'P')
                ? await self.validateAllocation()
                : true

            if (self.validateDetailForm() && isAllocationValid && confirm(`Mark the request as fulfilled?`)) {
                self.isDownloading(true)

                await self.saveChanges()

                $.ajax({
                    method: 'POST',
                    url: '/fulfillment',
                    data: {
                        requestId: self.selectedRequest().RequestId()
                    }
                }).done(res => {
                    if (res) {
                        let r = self.selectedRequest()

                        $.ajax({
                            method: 'POST',
                            url: '/fulfillment/delivery-receipt/' + self.selectedRequest().RequestType(),
                            data: {
                                tracking_number: r.TrackingNumber(),
                                beneficiary_name: r.Name(),
                                allocations: JSON.stringify(self.allocations()),
                                remarks: r.Remarks(),
                                receiver: r.Receiver(),
                                printing_date: moment().format('MM/DD/YYYY')
                            }
                        }).done(res => {
                            if (res) {
                                console.log('Downloading delivery receipt...')
                                window.open('/fulfillment/delivery-receipt/', '_blank')
                                
                                $('#request-details-modal').modal('close')
                                self.isDownloading(false)

                                self.allRequests.remove(request => {
                                    return self.selectedRequest().RequestId() == request.RequestId()
                                })

                                self.selectedRequest(new Request({}))
                                self.allocations([])
                                M.toast({ html: 'Request marked as fulfilled!', classes: 'teal', displayLength: 3500 })
                            }
                        })
                    }
                })
            } else if (!self.validateDetailForm()) {
                M.toast({
                    html: 'All fields are required!',
                    classes: 'yellow darken-3',
                    displayLength: 1750
                })
            } else if (!isAllocationValid) {
                M.toast({
                    html: 'Allocation value is invalid!',
                    classes: 'yellow darken-4',
                    displayLength: 2500
                })
            }
        }

        // Validation
        self.validateAllocation = async () => {
            let allocation = self.selectedRequest().Allocation()
            var result = false

            await $.ajax({
                method: 'GET',
                url: '/fulfillment/validate-allocation/' + allocation
            }).done(res => {
                if (res) {
                    result = true

                    self.allocations(res)
                }
            })

            return result
        }

        // Misc
    }

    ko.applyBindings(new FulfillmentViewModel())
})