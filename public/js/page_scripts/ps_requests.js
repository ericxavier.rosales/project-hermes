$(document).ready(() => {
    console.log('Requests page loaded!')
    
    function RequestsViewModel() {
        let self = this
        // Base View Model
        ko.BaseViewModel.call(self)

        // Selector
        const logRequestForm    = document.querySelector('#log-request-form')

        // Variables
        self.isValidating       = ko.observable(false)

        self.logClicked         = ko.observable(false)
        self.replenishmentCount = ko.observable(0)
        // Arrays

		// Computed
		self.filterRegion = ko.computed(() => {
			return (self.region() != '') ? self.region() : ''
		})

		self.filterProvince = ko.computed(() => {
			return (self.province() != '') ? self.province() : ''
		})

		self.filterCity = ko.computed(() => {
			return (self.city() != '') ? self.city() : ''
		})

        self.filteredRequests   = ko.pureComputed(() => {
            return ko.utils.arrayFilter(self.allRequests(), req => {
                let searchBarFilter = (self.searchBar() != '')
                    ? (req.Name()
                        .toUpperCase()
                        .replace(/[^a-zA-Z\d\s:]/g, '')
                        .indexOf(self.searchBar()
                            .toUpperCase()
                            .replace(/[^a-zA-Z\d\s:]/g, '')) != -1) ||
                        (req.TrackingNumber()
                        .toUpperCase()
                        .indexOf(self.searchBar()
                            .toUpperCase()) != -1)
                    : true
                
                let dateFilter = true
                let reqDate = moment(req.CreatedAt()).utcOffset(960)
                if (self.dateFilterOp() == 'eq' && self.dateFrom() != '') {
                    dateFilter = moment(reqDate).isSame(moment(self.dateFrom()), 'day')
                } else if (self.dateFilterOp() == 'lt' && self.dateTo() != '') {
                    dateFilter = moment(reqDate).isSameOrBefore(moment(self.dateTo()), 'day')
                } else if (self.dateFilterOp() == 'gt' && self.dateFrom() != '') {
                    dateFilter = moment(reqDate).isSameOrAfter(moment(self.dateFrom()), 'day')
                } else if (self.dateFilterOp() == 'bw' && self.dateTo() != '' && self.dateFrom() != '') {
                    dateFilter = moment(reqDate).isBetween(moment(self.dateFrom()), moment(self.dateTo()), null, '[]')
                } else {
                    dateFilter = true
                }

                let regionFilter = (self.filterRegion() != '') 
                    ? req.RegionId() == self.filterRegion()
                    : true

                let provinceFilter = (self.filterProvince() != '')
                    ? req.ProvinceId() == self.filterProvince()
                    : true

                let cityFilter = (self.filterCity() != '')
                    ? req.CityId() == self.filterCity()
                    : true

                return searchBarFilter && dateFilter && regionFilter && provinceFilter && cityFilter
            })
        })

        self.provinceEnabled    = ko.pureComputed(() => {
            return self.region() != ''
        })

        self.cityEnabled        = ko.pureComputed(() => {
            return self.provinceEnabled() && self.province()
        })

        self.sortedProvinces    = ko.pureComputed(() => {
            return ko.utils.arrayFilter(self.provinces(), province => {
                return province.region_id == self.filterRegion() || province.id == ''
            })
        })

        self.sortedCities       = ko.pureComputed(() => {
            return ko.utils.arrayFilter(self.cities(), city => {
                return city.province_id == self.filterProvince() || city.id == ''
            })
        })

        // Initialization
        self.tagFilter(0)
        $('#filter-sidenav-tag').attr('disabled', true)
        $('#filter-sidenav-tag').formSelect()
        $('#filter-sidenav-classification').attr('disabled', true)
        $('#filter-sidenav-classification').formSelect()

        // Listeners
        $('body').on('click', '#toast-delete', e => {
            M.Toast.dismissAll()
            self.selectedRequests([ self.selectedRequest().RequestId() ])
            self.deleteSelectedRequests()
            
            $('#details-beneficiary-type').formSelect()
            $('#details-request-type').formSelect()
            $('#request-details-modal').modal('close')
        })

        logRequestForm.addEventListener('submit', async (e) => {
            let formData = $('#log-request-form .validate')
            e.preventDefault()

            if (!self.logClicked()) {
                self.logClicked(true)
            } else {
                return
            }

            if (self.logClicked() && self.validateLogRequestForm(formData)) {
				let name = $('#beneficiary-name').val()
				let city = $('#city-select').val()
				let validationResult = await self.validateDuplicates(name, city)

				let duplicatePendingRequests = ko.utils.arrayFilter(validationResult.rows, row => {
					return row.tag < 5
				})
				let duplicateDeliveredRequests = ko.utils.arrayFilter(validationResult.rows, row => {
					return row.tag == 5 
				})

				let hasPending = duplicatePendingRequests.length > 0
				let hasDelivered = duplicateDeliveredRequests.length > 0 

				if (hasPending) {

					M.toast({html: `
						<p><strong>WARNING:</strong> That institution already exists in the database.</p>
					`, classes: 'red lighten-1'})

				} else if ((hasDelivered && confirm('WARNING: The institution already has a delivered request, and continuing this would count as a replenishment request. Continue?'))
					|| (!hasDelivered && confirm(`Are you sure you wish to log this request?`))) {

					self.logNewRequest()

				}
				self.logClicked(false)

            } else if (!self.validateLogRequestForm(formData)) {
                M.toast({
                    html: `All fields are required!`,
                    classes: 'yellow darken-3'
                })
                self.logClicked(false)
            }
        })

        // Methods / Functions 
        self.getRequestDetails = (data) => {
            self.selectedRequest(data)
            self.region(self.selectedRequest().RegionId())
            self.province(self.selectedRequest().ProvinceId())
            self.city(self.selectedRequest().CityId())
            
            M.updateTextFields()
            $('#details-beneficiary-type').formSelect()
            $('#details-beneficiary-classification').formSelect()
            $('#details-request-type').formSelect()
            M.textareaAutoResize($('#details-request-requests'))
            M.textareaAutoResize($('#details-request-remarks'))

            $('#request-details-modal').modal('open')
        }

        self.initializeLogRequestForm = () => {
            self.region('')
            $('#region-select').formSelect()
        }

        self.logNewRequest = () => {
            let formData = $('#log-request-form .validate')

            if (self.validateLogRequestForm(formData)) {
                $.ajax({
                    method: 'POST',
                    url: '/requests',
                    data: {
                        name: $('#beneficiary-name').val(),
                        contactPerson: $('#beneficiary-contact-person').val(),
                        contactNumber: $('#beneficiary-contact-number').val(),
                        city: $('#city-select').val()
                    }
                }).done(res => {
                    console.log(res)
                    if (res) {
                        console.log('Request logged successfully')
                        let newRequest = {
                            request_id: res.request_id,
                            beneficiary_id: res.beneficiary_id,
                            tracking_number: res.trackingNumber,
                            name: res.name,
                            location: res.location,
                            contact_person: res.contactPerson,
                            contact_number: res.contactNumber,
                            city_id: $('#city-select').val(),
                            city_name: $('#city-select option:selected').html(),
                            province_id: $("#province-select").val(),
                            province_name: $('#province-select option:selected').html(),
                            region_id: $('#region-select').val(),
                            region_name: $('#region-select option:selected').html(),
                            allocation: '',
                            remarks: '',
                            requests: '',
                            request_type: '',
                            type: '',
                            classification: '',
                            designation: '',
                            receiver: '', 
                            tag: 0
                        }
                        self.allRequests.unshift(new Request(newRequest))

                        self.clearLogRequestForm()
                        $('#add-modal').modal('close')
                        M.toast({html: `Request successfully logged with Tracking Number ${res.trackingNumber}!`, classes: 'teal'})
                    }
                }).fail(err => {
                    console.error(err)
                })
            }
            self.logClicked(false)
		}

        self.confirmDetailsForm = async () => {
            if (!self.validateDetailForm()) {
                M.toast({
                    html: `All fields are required!`,
                    classes: 'yellow darken-3'
                })

                return
            }

			self.isValidating(true)
			let validationResult = await self.validateDuplicates(self.selectedRequest().Name(), self.city())
			let duplicatePendingRequests = ko.utils.arrayFilter(validationResult.rows, row => {
				return row.id != self.selectedRequest().BeneficiaryId() && row.tag < 5
			})
			let duplicateDeliveredRequests = ko.utils.arrayFilter(validationResult.rows, row => {
				return row.id != self.selectedRequest().BeneficiaryId() && row.tag == 5 
			})
			self.replenishmentCount(duplicateDeliveredRequests.length)

			let hasPending = duplicatePendingRequests.length > 0
			let hasDelivered = duplicateDeliveredRequests.length > 0 
			self.isValidating(false)

			if (hasPending) {
				M.toast({
					html: `
						<p><strong>WARNING:</strong> That institution already has a pending request that has not been delivered.<br>You may choose to delete this request.</p>
						<a id='toast-delete' class='btn-flat toast-action'>DELETE</a>
					`, 
					classes: 'red lighten-1',
					displayLength: 5500
				})
			} else if ((hasDelivered && confirm('WARNING: The institution already has a delivered request, and continuing this would count as a replenishment request. Continue?'))
				|| (!hasDelivered && confirm(`Are you sure you wish to confirm this request?`))) {
				let regionName = $('#details-region-select option:selected').html()
				let provinceName = $('#details-province-select option:selected').html()
				let cityName = $('#details-city-select option:selected').html()
				self.selectedRequest().RegionName(regionName)
				self.selectedRequest().RegionId(self.region())
				self.selectedRequest().ProvinceName(provinceName)
				self.selectedRequest().ProvinceId(self.province())
				self.selectedRequest().CityName(cityName)
				self.selectedRequest().CityId(self.city())
				self.selectedRequest().Location(`${cityName}, ${provinceName}`)
				if (hasDelivered) {
					self.selectedRequest().Remarks(`[REPLENISHMENT REQUEST: ${self.replenishmentCount()}] ${self.selectedRequest().Remarks()}`)
				}

				$.ajax({
					method: 'PUT',
					url: '/requests/' + self.selectedRequest().RequestId(),
					data: self.selectedRequest()
				}).done(res => {
					console.log(res)

					if (res) {
						$('#request-details-modal').modal('close')
						M.toast({ html: 'Request confirmed successfully!', classes: 'teal'})   

						self.allRequests.remove(request => {
							return self.selectedRequest().RequestId() == request.RequestId()
						})

						self.selectedRequest(new Request({}))
						$('#details-beneficiary-type').formSelect()
						$('#details-beneficiary-classification').formSelect()
						$('#details-request-type').formSelect()
					}
				})
			} 
        }

        self.confirmSaveChanges = async () => {
			self.isValidating(true)

			let validationResult = await self.validateDuplicates(self.selectedRequest().Name(), self.city())
			
			let duplicatePendingRequests = ko.utils.arrayFilter(validationResult.rows, row => {
				return row.id != self.selectedRequest().BeneficiaryId() && row.tag < 5
			})

			let hasPending = duplicatePendingRequests.length > 0
			self.isValidating(false)

			if (hasPending) {
				M.toast({
					html: `
						<p><strong>WARNING:</strong> That institution already has a pending request that has not been delivered.<br>You may choose to delete this request.</p>
						<a id='toast-delete' class='btn-flat toast-action'>DELETE</a>
					`, 
					classes: 'red lighten-1',
					displayLength: 5500
				})
			} else if (confirm("Save these changes?")) {
				let regionName = $('#details-region-select option:selected').html()
				let provinceName = $('#details-province-select option:selected').html()
				let cityName = $('#details-city-select option:selected').html()
				self.selectedRequest().RegionName(regionName)
				self.selectedRequest().RegionId(self.region())
				self.selectedRequest().ProvinceName(provinceName)
				self.selectedRequest().ProvinceId(self.province())
				self.selectedRequest().CityName(cityName)
				self.selectedRequest().CityId(self.city())
				self.selectedRequest().Location(`${cityName}, ${provinceName}`)

				self.saveChanges()
				
				self.selectedRequest(new Request({}))

				$('#request-details-modal').modal('close')
				M.toast({ html: 'Request updated successfully!', classes: 'teal'})   
			}

        }
        
        // Validation
        self.validateLogRequestForm = (formData) => {
            let elementCount = formData.length
            let hasCompleteValues = true
            for (let i = 0; i < elementCount; i++) {
                if (formData[i].value == '') {
                    hasCompleteValues = false
                    break
                }
            }
            
            if (!hasCompleteValues && !(self.provinceEnabled() && self.cityEnabled() && formData[5].value != '')) {
                $('#location-validation-msg').fadeIn(250)
                $('#location-validation-msg').fadeOut(5000)
            }

            return hasCompleteValues
        }

        // Misc 
        self.clearLogRequestForm = () => {
            $('#beneficiary-name').val('')
            $('#beneficiary-contact-person').val('')
            $('#beneficiary-contact-number').val('')
            self.region('')
            $('#region-select').formSelect()
        }
    }

    ko.applyBindings(new RequestsViewModel())
})
