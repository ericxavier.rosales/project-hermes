$(document).ready(() => {
    console.log('Delivery page loaded!')

    function DeliveryViewModel() {
        let self = this
        // Base View Model
        ko.BaseViewModel.call(self)
        // Variables 

        // Arrays 

        // Initialization 
        self.tagFilter(4)
        $('#filter-sidenav-tag').attr('disabled', true)
        $('#filter-sidenav-tag').formSelect()

        // Computed 
        
        // Listeners

        // Methods 
        self.deliverRequest = async () => {
            if (self.validateDetailForm() && confirm(`Mark the request as delivered?`)) {
                // Save changes first
                await self.saveChanges()

                $.ajax({
                    method: 'POST',
                    url: '/delivery',
                    data: {
                        requestIds: [ self.selectedRequest().RequestId() ]
                    }
                }).done(res => {
                    if (res) {
                        $('#request-details-modal').modal('close')

                        self.allRequests.remove(request => {
                            return self.selectedRequest().RequestId() == request.RequestId()
                        })

                        self.selectedRequest(new Request({}))
                        M.toast({ html: 'Request marked as delivered!', classes: 'teal' })
                    }
                })
            } else if (!self.validateDetailForm()) {
                M.toast({
                    html: 'All fields are required!',
                    classes: 'yellow darken-3',
                    displayLength: 1750
                })
            }
        }

        self.deliverSelectedRequests = () => {
            let selectedCount = self.selectedRequests().length
            if (selectedCount && confirm('Mark the selected request(s) as delivered?')) {
                $.ajax({
                    method: 'POST',
                    url: '/delivery',
                    data: {
                        requestIds: self.selectedRequests()
                    }
                }).done(res => {
                    if(res) {

                        self.allRequests.remove(request => {
                            return self.selectedRequests.indexOf(request.RequestId()) > -1
                        })

                        self.selectedRequests([])
                        M.toast({ html: 'Request(s) marked as delivered!', classes: 'teal' })
                    }
                })
            } else if (selectedCount == 0) {
                M.toast({ 
                    html: 'No requests selected!', 
                    classes: 'yellow darken-3', 
                    displayLength: 2000 
                })
            }
        }

        // Validation

        // Misc
        self.getRequestDetails = (data) => {
            self.selectedRequest(data)
            
            M.updateTextFields()
            M.textareaAutoResize($('#details-request-requests'))
            M.textareaAutoResize($('#details-request-remarks'))

            $('#request-details-modal').modal('open')
        }
    }

    ko.applyBindings(new DeliveryViewModel())
})