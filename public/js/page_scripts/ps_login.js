$(document).ready(() => {
    console.log('Welcome to Hermes!')

    function LoginViewModel() {
        let self = this
        // Selector
        const loginForm = document.querySelector('#login-form')

        // Observables
        self.username = ko.observable('')
        self.password = ko.observable('')

        // Listener
        loginForm.addEventListener('submit', e => {
            e.preventDefault()
            self.login(self.username(), self.password())
        })

        // Functions
        self.login = (user, pass) => {
            console.log('Logging in...')
            $.ajax({
                method: 'POST',
                url: '/login/authenticate',
                data: {
                    user: user,
                    pass: pass 
                }
            }).done(res => {
                if (res) {
                    M.toast({
                        html: `Login success! Taking you to the home page...`,
                        classes: 'teal'
                    })
                    window.location.href = '/'
                } else {
                    M.toast({
                        html: `Invalid username and/or password!`,
                        classes: 'red'
                    })
                }
            })
        }
    }

    ko.applyBindings(new LoginViewModel())
})