$(document).ready(() => {
    // Initialize page components
    setSidebarActiveRoute()
    initializeMaterializeComponents()

    // Base viewModel
    ko.BaseViewModel = function() {
        let self = this
        // Selectors
		var sidenavSlideout     = document.querySelector('#filter-sidenav-slideout')
		var requestDetailsModal = document.querySelector('#request-details-modal')
		var addModal 			= document.querySelector('#add-modal')
        M.Sidenav.init(sidenavSlideout, {
            edge: 'right',
            onOpenStart: (el) => {
                $('#filter-sidenav-tag').formSelect()
            }, 
            onCloseStart: (el) => {
                $('#filter-sidenav-tag').formSelect()
            }
		})
		M.Modal.init(addModal, {
			onCloseEnd: () => {
				self.region('')
				self.province('')
				self.city('')
			}
		})
		M.Modal.init(requestDetailsModal, {
			onCloseEnd: () => {
				self.region('')
				self.province('')
				self.city('')
			}
		})
        
        // Variables 
        self.region             = ko.observable()
        self.province           = ko.observable()
        self.city               = ko.observable()

        self.searchBar          = ko.observable('')
        self.tagFilter          = ko.observable(6)
        self.typeFilter         = ko.observable('')
        self.dateFilterOp       = ko.observable('eq')
        self.dateFrom           = ko.observable('')
        self.dateTo             = ko.observable('')

        self.allSelected        = ko.observable(false)
        self.isReportsAccount   = ko.observable(role == 'RA')
        self.isCallCenter       = ko.observable(role == 'CC')
        self.isSuperUser        = ko.observable(role == 'SU')
        self.selectedRequest    = ko.observable(new Request({}))

        // Arrays 
        self.regions            = ko.observableArray([{id:'', name:' -- Choose -- '}].concat(allRegions))
        self.provinces          = ko.observableArray([{id:'', name:' -- Choose -- ', region_id: ''}].concat(allProvinces))
        self.cities             = ko.observableArray([{id:'', name:' -- Choose -- ', province_id: ''}].concat(allCities))

        self.allRequests        = ko.observableArray([])
        self.selectedRequests   = ko.observableArray([])

        // Initialization 
        allTotalRequests.forEach(req => {
            self.allRequests.push(new Request(req))
        })

        // Computed 
        self.viewTitle          = ko.computed(() => {
            let tags = [ 'New', 'Confirmed', 'Approved', 'Fulfilled', 'In Transit', 'Delivered', 'All' ]
            let classification = (self.typeFilter() != '') 
                ? self.typeFilter() + ' '
                : ''
            return `${tags[self.tagFilter()]} ${classification}Requests`
        })

        self.filteredRequests   = ko.pureComputed(() => {
            return ko.utils.arrayFilter(self.allRequests(), req => {
                let searchBarFilter = (self.searchBar() != '')
                    ? (req.Name()
                        .toUpperCase()
                        .replace(/[^a-zA-Z\d\s:]/g, '')
                        .indexOf(self.searchBar()
                            .toUpperCase()
                            .replace(/[^a-zA-Z\d\s:]/g, '')) != -1) ||
                        (req.TrackingNumber()
                        .toUpperCase()
                        .indexOf(self.searchBar()
                            .toUpperCase()) != -1)
                    : true

                let tagFilter = (self.tagFilter() == 6)
                    ? true
                    : req.Tag() == self.tagFilter()
                
                let typeFilter = (self.typeFilter() == '')
                    ? true
                    : req.Classification() == self.typeFilter()
                
                let dateFilter = true
                let reqDate = moment(req.CreatedAt()).utcOffset(960)
                if (self.dateFilterOp() == 'eq' && self.dateFrom() != '') {
                    dateFilter = moment(reqDate).isSame(moment(self.dateFrom()), 'day')
                } else if (self.dateFilterOp() == 'lt' && self.dateTo() != '') {
                    dateFilter = moment(reqDate).isSameOrBefore(moment(self.dateTo()), 'day')
                } else if (self.dateFilterOp() == 'gt' && self.dateFrom() != '') {
                    dateFilter = moment(reqDate).isSameOrAfter(moment(self.dateFrom()), 'day')
                } else if (self.dateFilterOp() == 'bw' && self.dateTo() != '' && self.dateFrom() != '') {
                    dateFilter = moment(reqDate).isBetween(moment(self.dateFrom()), moment(self.dateTo()), null, '[]')
                } else {
                    dateFilter = true
                }

                let regionFilter = (self.region() != '') 
                    ? req.RegionId() == self.region()
                    : true

                let provinceFilter = (self.province() != '')
                    ? req.ProvinceId() == self.province()
                    : true

                let cityFilter = (self.city() != '')
                    ? req.CityId() == self.city()
                    : true

                return tagFilter && typeFilter && searchBarFilter && dateFilter && regionFilter && provinceFilter && cityFilter
            })
        })

        self.filteredProvinces  = ko.pureComputed(() => {
            return ko.utils.arrayFilter(self.provinces(), (province, index) => {
                return province.region_id == self.region() || province.id == ''
            })
        })

        self.filteredCities     = ko.pureComputed(() => {
            return ko.utils.arrayFilter(self.cities(), (city, index) => {
                return city.province_id == self.province() || city.id == ''
            })
        })


        // Methods
        self.rowSelect = (data) => {
            if (data.IsChecked()) {
                self.selectedRequests.push(data.RequestId())
            } else {
                let index = ko.utils.arrayIndexOf(self.selectedRequests(), data.RequestId())
                self.selectedRequests.splice(index, 1)
            }

            return true
        }

        self.selectAll = () => {
            self.selectedRequests([])
            if (self.allSelected()) {
                ko.utils.arrayForEach(self.filteredRequests(), req => {
                    req.IsChecked(true)
                    self.selectedRequests.push(req.RequestId())
                })
            } else {
                ko.utils.arrayForEach(self.filteredRequests(), req => {
                    req.IsChecked(false)
                })
            }

            return true
        }

        self.saveChanges = async () => {
            let result = false 

            await $.ajax({
                method: 'PATCH',
                url: '/requests/' + self.selectedRequest().RequestId(),
                data: JSON.parse(ko.toJSON(self.selectedRequest()))
            }).done(res => {
                console.log(res)

                result = true
            })

            return result 
        }

        self.confirmSaveChanges = async () => {
            if (self.validateDetailForm() && confirm("Save these changes?")) {
                let result = await self.saveChanges()
                if (result) {
                    self.selectedRequest(new Request({}))

                    $('#request-details-modal').modal('close')
                    M.toast({ html: 'Request updated successfully!', classes: 'teal'})   
                }
            } else if (!self.validateDetailForm()) {
                M.toast({
                    html: 'All fields are required!',
                    classes: 'yellow darken-3',
                    displayLength: 1750
                })
            }
        }

        self.deleteSelectedRequests = () => {
            let selectedCount = self.selectedRequests().length
            if (selectedCount && confirm('Are you sure you want to delete the selected request(s)?')) {
                $.ajax({
                    method: 'DELETE',
                    url: '/requests',
                    data: {
                        requestIds: self.selectedRequests()
                    } 
                }).done(res => {
                    if (res) {

                        self.allRequests.remove(request => {
                            return self.selectedRequests.indexOf(request.RequestId()) > -1
                        })

                        self.selectedRequests([])
                        M.toast({ html: 'Request(s) deleted!', classes: 'red' })
                    }
                })
            } else if (selectedCount == 0) {
                M.toast({ 
                    html: 'No requests selected!', 
                    classes: 'yellow darken-3', 
                    displayLength: 2000 
                })
            }
        } 

		// Validation
		self.validateDuplicates = (name, city) => {
			return $.ajax({
				method: 'POST', 
				url: '/requests/validate',
				data: {
					name: name,
					city: city
				}
			})
		}

        // Misc
        self.getRequestDetails = (data) => {
            self.selectedRequest(data)
            
            M.updateTextFields()
            $('#details-beneficiary-type').formSelect()
            $('#details-beneficiary-classification').formSelect()
            $('#details-request-type').formSelect()
            M.textareaAutoResize($('#details-request-requests'))
            M.textareaAutoResize($('#details-request-remarks'))

            $('#request-details-modal').modal('open')
        }

        self.validateDetailForm = () => {
			let r = self.selectedRequest()
            return (
                r.Type() != '' &&
                r.Classification() != '' &&
                r.Designation() != '' &&
                r.ContactPerson() != '' &&
                r.ContactNumber() != '' && 
                r.RegionId() != '' && 
                r.RequestType() != '' &&
                r.Allocation() != 0 && 
                r.Receiver() != '' && 
                r.Requests() != '' && 
                r.Remarks() != ''
            )
        }

        self.clearFilterValues = () => {
            // Clear location filters
            self.region('')
            self.province('')
            self.city('')

            // Clear search bar
            self.searchBar('')

            // Clear date filters
            self.dateFilterOp('eq')
            self.dateFrom('')
            self.dateTo('')

            // Clear tag and type filters
            self.typeFilter('')
            $('select').formSelect()
        }
    }
})

initializeMaterializeComponents = () => {
    $('select').formSelect()
    $('.tabs').tabs()
    $('.modal').modal()
    $('.sidenav').sidenav()
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        showClearBtn: true,
        container: $('body')
    })
    $('.tooltipped').tooltip()
    $('.collapsible').collapsible()
    $('.dropdown-trigger').dropdown({
        constrainWidth: true
    })
    $('.fixed-action-btn').floatingActionButton({
        direction: 'left',
        hoverEnabled: false
    })
}

setSidebarActiveRoute = () => {
    let url = $(location).attr('href')
    let route = url.substr(url.lastIndexOf('/') + 1)
    route = route == '' ? 'overview' : route
    $(`#${route}-li`).addClass('active')
}

generateTimestamp = () => {
    let date = new Date()
    let ts = ((date.getUTCMonth() + 1) + '').padStart(2, '0') + (date.getUTCDate() + '').padStart(2, '0') + date.getUTCFullYear() + '_'
        ts += (date.getUTCHours() + 8 + '').padStart(2, '0') + (date.getUTCMinutes() + '').padStart(2, '0') + (date.getUTCSeconds() + '').padStart(2, '0')
    
    return ts
}