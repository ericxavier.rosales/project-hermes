$(document).ready(() => {
	console.log('Index page loaded!')

    if (firstLogin) {
        console.log('Redirecting to user management page to change password...')
        window.location.href = '/users'
    }

    function OverviewModel() {
        let self = this
        // Base View Model
		ko.BaseViewModel.call(self)

		// Variables
		self.canEditRecords 	= role == 'AD' || role == 'SU'
		self.canEditDelivery 	= role != 'CC' && role != 'RA'

		self.canSaveEdits 		= ko.observable(false)

		self.selectedDate 		= ko.observable('')
		self.selectedTime		= ko.observable('')

        // Computed 
        self.filterDetails      = ko.pureComputed(() => {
            let searchBarFilter = (self.searchBar() != '')
                ? `With Name(s)/Tracking Number(s) containing '${self.searchBar()}' \n`
                : ''

            let dateFilter = ''
            if (self.dateFilterOp() == 'eq' && self.dateFrom() != '') {
                dateFilter = `With Date(s) equal to ${moment(self.dateFrom()).format('MMMM D, YYYY')} \n`
            } else if (self.dateFilterOp() == 'lt' && self.dateTo() != '') {
                dateFilter = `With Date(s) on or before ${moment(self.dateTo()).format('MMMM D, YYYY')} \n`
            } else if (self.dateFilterOp() == 'gt' && self.dateFrom() != '') {
                dateFilter = `With Date(s) on or after ${moment(self.dateFrom()).format('MMMM D, YYYY')} \n`
            } else if (self.dateFilterOp() == 'bw' && self.dateTo() != '' && self.dateFrom() != '') {
                dateFilter = `With Date(s) on or between ${moment(self.dateFrom()).format('MMMM D, YYYY')} and ${moment(self.dateTo()).format('MMMM D, YYYY')} \n`
            } else {
                dateFilter = ''
            }

            let regionFilter = ''
            if (self.region() != '' && self.region() != null) {
                let region = ko.utils.arrayFirst(allRegions, reg => {
                    return reg.id == self.region()
                })
                regionFilter = `Region: ${region.name}`
            }

            let provinceFilter = ''
            if (self.province() != '' && self.province() != null) {
                let province = ko.utils.arrayFirst(self.filteredProvinces(), province => {
                    return province.id == self.province()
                })
                provinceFilter = `Province: ${province.name}`
            }

            let cityFilter = '' 
            if (self.city() != '' && self.city() != null) {
                let city = ko.utils.arrayFirst(self.filteredCities(), city => {
                    return city.id == self.city()
                })
                cityFilter = `City: ${city.name}`
            }


            let locationFilter = (regionFilter != '' || provinceFilter != '' || cityFilter != '')
                ? `With location in ${regionFilter} ${provinceFilter} ${cityFilter}`
                : ''

            return searchBarFilter + dateFilter + locationFilter
        })

		// Listeners
		if (self.canEditRecords) {
			$('.editable').attr('disabled', false)
			self.canSaveEdits(true)
		} 

        // Methods 
        self.saveChanges = async () => {
			let result = false 

            await $.ajax({
                method: 'PATCH',
                url: '/requests/' + self.selectedRequest().RequestId(),
                data: JSON.parse(ko.toJSON(self.selectedRequest()))
            }).done(res => {
                console.log(res)

                result = true
            })

            return result 
        }

        // Validation

        // Misc
        self.exportRequestsAsXls = () => {
            let tempRequests = []
            let exportDate = [{ 
                A : 'Export Date:', 
                B : moment().format('MM-DD-YYYY')
            }]
            let exportFilter = [{
                A : 'Export Filter:',
                B : self.viewTitle() + ' ' + self.filterDetails()
            }]
            let headers = [{ 
                A : 'Tracking Number', 
                B : 'Request Date', 
                C : 'Contact Person', 
                D : 'Designation', 
                E : 'City', 
                F : 'Province', 
                G : 'Region', 
                H : 'Beneficiary Name',
                I : 'Type', 
                J : 'Classification',
                K : 'Contact Person', 
                L : 'Request Type', 
                M : 'Requests', 
                N : 'Allocation', 
                O : 'Remarks', 
                P : 'Receiver',
                Q : 'Delivery Date'
            }]
            let merge = [
                { 
                    s : { r : 1, c : 0 }, 
                    e : { r : 2, c : 0 }
                },
                {
                    s : { r : 1, c : 1 },
                    e : { r : 2, c : 16 }
                }
            ]

            ko.utils.arrayForEach(self.filteredRequests(), req => {
                tempRequests.push({
                    TrackingNumber: req.TrackingNumber(),
                    RequestDate: moment(req.CreatedAt()).utcOffset(960).format('MM-DD-YYYY'),
                    ContactPerson: req.ContactPerson(),
                    Designation: req.Designation(),
                    City: req.CityName(),
                    Province: req.ProvinceName(),
                    Region: req.RegionName(),
                    BeneficiaryName: req.Name(),
                    Type: req.Type(),
                    Classification: req.Classification(),
                    ContactNumber: `'${req.ContactNumber()}`,
                    RequestType: req.RequestType(),
                    Requests: req.Requests(),
                    Allocation: req.Allocation(),
                    Remarks: req.Remarks(),
                    Receiver: req.Receiver(),
                    DeliveryDate: req.DeliveryDate()
                })
            })

            let filename = `Approval-Export-${generateTimestamp()}`
            let wb = XLSX.utils.book_new()
            let ws = XLSX.utils.json_to_sheet(exportDate, { skipHeader: true })
            XLSX.utils.sheet_add_json(ws, exportFilter, { origin: 'A2', skipHeader: true })
            XLSX.utils.sheet_add_json(ws, headers, { origin: 'A5', skipHeader: true })
            XLSX.utils.sheet_add_json(ws, tempRequests, { origin: 'A6', skipHeader: true })
            ws['!merges'] = merge
            XLSX.utils.book_append_sheet(wb, ws)
            XLSX.writeFile(wb, `${filename}.xls`)

            M.toast({
                html: `Successfully exported ${filename}!`,
                classes: 'teal'
            })
            tempRequests = null
        }

        self.getRequestDetails = (data) => {
			self.selectedRequest(data)
			
			if (self.canEditDelivery && self.selectedRequest().Tag() == 5) {
				$('#details-request-receiver').attr('disabled', false)
				$('#details-request-delivery-date').attr('disabled', false)
				self.canSaveEdits(true)
			} else {
				self.canSaveEdits(self.canEditRecords)
			}
            
            M.updateTextFields()
			$('#details-beneficiary-type').formSelect()
			$('#details-beneficiary-classification').formSelect()
            $('#details-request-type').formSelect()
            M.textareaAutoResize($('#details-request-requests'))
            M.textareaAutoResize($('#details-request-remarks'))

            $('#request-details-modal').modal('open')
        }
    }

    ko.applyBindings(new OverviewModel())
})