const { Pool } = require('pg')
const dbConfig = require('./db_settings')

const pool = new Pool({
    user     : dbConfig.user,
    password : dbConfig.password,
    host     : dbConfig.host,
    port     : dbConfig.port,
    database : dbConfig.database
})

module.exports = {
    query: (text, params, callback) => {
        const start = Date.now()
        return pool.query(text, params, (err, res) => {
            const duration = Date.now() - start
            console.log('Executed query', { text, duration, rows: res.rowCount })
            callback(err, res)
        })
    },
    asyncQuery: (text, params) => pool.query(text, params), 
    getClient: () => { return pool.connect() },
    schema: dbConfig.schema
}