WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('NCR - National Capital Region') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Metro Manila' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('CAR - Cordillera Admininstrative Region') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Abra' from rows union
select id, 'Apayao' from rows union
select id, 'Benguet' from rows union
select id, 'Ifugao' from rows union
select id, 'Kalinga' from rows union
select id, 'Mountain Province' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('I - Ilocos Region') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Ilocos Norte' from rows union
select id, 'Ilocos Sur' from rows union
select id, 'La Union' from rows union
select id, 'Pangasinan' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('II - Cagayan Valley') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Batanes' from rows union
select id, 'Cagayan' from rows union
select id, 'Isabela' from rows union
select id, 'Nueva Vizcaya' from rows union
select id, 'Quirino' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('III - Central Luzon') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Aurora' from rows union
select id, 'Bataan' from rows union
select id, 'Bulacan' from rows union
select id, 'Nueva Ecija' from rows union
select id, 'Pampanga' from rows union
select id, 'Tarlac' from rows union
select id, 'Zambales' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('IV-A - CALABARZON') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Batangas' from rows union
select id, 'Cavite' from rows union
select id, 'Laguna' from rows union
select id, 'Quezon' from rows union
select id, 'Rizal' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('IV-B - MIMAROPA') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Marinduque' from rows union
select id, 'Occidental Mindoro' from rows union
select id, 'Oriental Mindoro' from rows union
select id, 'Palawan' from rows union
select id, 'Romblon' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('V - Bicol Region') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Albay' from rows union
select id, 'Camarines Norte' from rows union
select id, 'Camarines Sur' from rows union
select id, 'Catanduanes' from rows union
select id, 'Masbate' from rows union
select id, 'Sorsogon' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('VI - Western Visayas') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Aklan' from rows union
select id, 'Antique' from rows union
select id, 'Capiz' from rows union
select id, 'Guimaras' from rows union
select id, 'Iloilo' from rows union
select id, 'Negros Occidental' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('VII - Central Visayas') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Bohol' from rows union
select id, 'Cebu' from rows union
select id, 'Negros Oriental' from rows union
select id, 'Siquijor' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('VIII - Eastern Visayas') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Biliran' from rows union
select id, 'Eastern Samar' from rows union
select id, 'Leyte' from rows union
select id, 'Northern Samar' from rows union
select id, 'Samar' from rows union
select id, 'Southern Leyte' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('IX - Zamboange Peninsula') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Zamboanga del Norte' from rows union
select id, 'Zamboanga del Sur' from rows union
select id, 'Zamboanga Sibugay' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('X - Northen Mindanao') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Bukidnon' from rows union
select id, 'Camiguin' from rows union
select id, 'Lanao del Norte' from rows union
select id, 'Misamis Occidental' from rows union
select id, 'Misamis Oriental' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('XI - Davao Region') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Compostela Valley' from rows union
select id, 'Davao del Norte' from rows union
select id, 'Davao del Sur' from rows union
select id, 'Davao Occidental' from rows union
select id, 'Davao Oriental' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('XII - SOCCSKARGEN') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Cotabato' from rows union
select id, 'Sarangani' from rows union
select id, 'South Cotabato' from rows union
select id, 'Sultan Kudarat' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('XIII - Caraga') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Agusan del Norte' from rows union
select id, 'Agusan del Sur' from rows union
select id, 'Dinagat Islands' from rows union
select id, 'Surigao del Norte' from rows union
select id, 'Surigao del Sur' from rows;

WITH rows AS (
    INSERT INTO hermes_dev.regions(name) values('ARMM - Autonomous Region in Muslim Mindanao') RETURNING id
)
INSERT INTO hermes_dev.provinces(region_id, name)
select id, 'Basilan' from rows union
select id, 'Lanao del Sur' from rows union
select id, 'Maguindanao' from rows union
select id, 'Sulu' from rows union
select id, 'Tawi-tawi' from rows;