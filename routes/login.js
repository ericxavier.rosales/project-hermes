var express = require('express');
var router = express.Router();

const db     = require('../db')
const bcrypt = require('bcrypt')
const moment = require('moment')

router.get('/', async (req, res, next) => {
    res.render('login', {
        title: 'Project HERMeS - Login'
    })
})

router.post('/authenticate', async (req, res, next) => {
    let { user, pass } = req.body

    let loginQry = `
        SELECT username, password, role, last_login
        FROM ${db.schema}.users
        WHERE username = $1
    `
    let loginValues = [ user ]
    let hashPwResult = await db.asyncQuery(loginQry, loginValues)
    if (hashPwResult.rowCount == 0) {
        res.send(false)
        return
    }

    let retrievedUser = hashPwResult.rows[0]
    let hashedPw = hashPwResult.rows[0].password
    let firstLogin = retrievedUser.last_login == null

    let result = bcrypt.compareSync(pass, hashedPw)

    // Login success
    if (result) {
        console.log('Login success!')
        req.session.user = { 
            username: user, 
            role: retrievedUser.role, 
            first_login: firstLogin,
            login_date: moment().format('YYYY-MM-DD HH:mm:ss')
        }

        console.log('Updating...')
        // Update user last_login date 
        let updateLoginDateQry = `
            UPDATE ${db.schema}.users
            SET last_login = NOW()
            WHERE username = '${user}'
        `
        await db.asyncQuery(updateLoginDateQry)

        res.send(true)
    } else {
        res.send(false)
    }
})

router.post('/change-password', async (req, res, next) => {
    let { oldPass, newPass } = req.body
    let user = req.session.user.username

    let getPasswordQry = `
        SELECT password
        FROM ${db.schema}.users
        WHERE username = $1
    `
    let getPasswordValues = [ user ]
    let getPasswordResult = await db.asyncQuery(getPasswordQry, getPasswordValues)
    let oldHashedPassword = getPasswordResult.rows[0].password

    let oldPassMatch = bcrypt.compareSync(oldPass, oldHashedPassword)

    if (oldPassMatch) {
        let hashedPassword = bcrypt.hashSync(newPass, 10)

        let updatePassQry = `
            UPDATE ${db.schema}.users
            SET password = $1
            WHERE username = $2
        `
        let updatePassValues = [ hashedPassword, user ]
        let updateResult = await db.asyncQuery(updatePassQry, updatePassValues)

        if (updateResult) {
            // Password change success, redirect to login
            res.send(true)
        } else {
            // Error occurred on update
            res.send(false)
        }

    } else {
        // Old passwords do not match!
        res.send(false)
    }
})

router.post('/create-user', async (req, res, next) => {
    let { user, pass, role } = req.body

    let checkExistingQry = `
        SELECT id, username
        FROM ${db.schema}.users
        WHERE username = $1
    `
    let checkExistingValues = [ user ]
    let checkResult = await db.asyncQuery(checkExistingQry, checkExistingValues)

    // Has existing user with the same username
    if (checkResult.rowCount > 0) {
        res.send({
            createResult: -1
        })
        return 
    }

    let hashedPassword = bcrypt.hashSync(pass, 10)
    let createQry = `
        INSERT INTO ${db.schema}.users(username, password, role)
        VALUES($1, $2, $3)
    `
    let createValues = [ user, hashedPassword, role ]

    try {
        await db.asyncQuery(createQry, createValues)
    } catch(e) {
        console.error(e)
        res.send({
            createResult: 0
        })
        throw e
    } 

    res.send({
        createResult: 1
    })
})

module.exports = router