var express = require('express')
var router = express.Router()

router.get('/', async (req, res, next) => {
    if (req.session.user && req.cookies.user_sid) {
        res.clearCookie('user_sid')
        req.session.destroy()
        res.redirect('/')
    } else {
        res.redirect('/login')
    }
})

module.exports = router