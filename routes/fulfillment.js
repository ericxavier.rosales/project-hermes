var express = require('express');
var router = express.Router();

const db    = require('../db')
const ejs   = require('ejs')
const pdf   = require('html-pdf')
const fs    = require('fs')
const path  = require('path')

var downloadFilepath = ''
var downloadFilename = ''

/* GET home page. */
router.get('/', async (req, res, next) => {
    if (req.session.user && req.cookies.user_sid) {
        if (req.session.user.role != 'CC' && req.session.user.role != 'RA') {
            let approvedRequestsQry = `
                SELECT r.id request_id, r.tracking_number, b.id beneficiary_id, b.name, 
                    r.allocation, r.request_type, r.requests, r.remarks, r.receiver,
                    b.designation, b.type, b.contact_person, b.contact_number, b.classification,
                    c.id city_id, c.name city_name, p.id province_id, p.name province_name, rg.id region_id, rg.name region_name,
                    CONCAT(c.name || ', ' || p.name) AS location, r.tag, r.created_at, date(r.created_at) request_date, r.date_delivered
                FROM ${db.schema}.beneficiaries b, ${db.schema}.requests r, ${db.schema}.cities c, ${db.schema}.provinces p, ${db.schema}.regions rg
                WHERE 1=1 
                AND r.beneficiary_id = b.id
                AND b.city_id = c.id
                AND c.province_id = p.id
                AND p.region_id = rg.id
                AND r.status = 'A'
                AND r.tag = 2
                ORDER BY r.created_at DESC`
            let allRegionsQuery = `SELECT id, name FROM ${db.schema}.regions`
            let allProvincesQuery = `SELECT id, name, region_id FROM ${db.schema}.provinces`
            let allCitiesQuery = `SELECT id, name, province_id FROM ${db.schema}.cities`
        
            let requestQueryResult = await db.asyncQuery(approvedRequestsQry)
            let requests = requestQueryResult.rows
        
            let regionsQueryResult = await db.asyncQuery(allRegionsQuery)
            let allRegions = regionsQueryResult.rows
        
            let provincesQueryResult = await db.asyncQuery(allProvincesQuery)
            let allProvinces = provincesQueryResult.rows
        
            let citiesQueryResult = await db.asyncQuery(allCitiesQuery)
            let allCities = citiesQueryResult.rows
        
            res.render('fulfillment', { 
                title: 'Project HERMeS - Fulfillment',
                username: req.session.user.username,
                role: req.session.user.role,
                allRequests: requests,
                allRegions: allRegions,
                allProvinces: allProvinces,
                allCities: allCities
            })
        } else {
            res.redirect('/')
        }
    } else {
        res.redirect('/login');
    }
})

/* POST */
router.post('/', async (req, res, next) => {
    let requestId = req.body.requestId

    let fulfillRequestsQry = `
        UPDATE ${db.schema}.requests 
        SET tag = 3,
        updated_at = NOW()
        WHERE id = ${requestId}
    `

    try {
        let fulfillResult = await db.asyncQuery(fulfillRequestsQry)
    } catch(e) {
        console.error(e)
        res.send(false)
        throw e 
    } 
    
    res.send(true)
})

/* ALLOCATION VALIDATION */ 
router.get('/validate-allocation/:allocation', async (req, res, next) => {
    let allocation = req.params.allocation

    let validationQry = `
        SELECT allocation, description, unit, quantity
        FROM ${db.schema}.allocations
        WHERE allocation = ${allocation}
        ORDER BY id
    `

    try {
        let validationResult = await db.asyncQuery(validationQry)

        res.send((validationResult.rowCount > 0) 
            ? validationResult.rows 
            : false)
    } catch(e) {
        console.error(e.stack)
        res.send(false)
        throw e
    }
})

/* DELIVERY RECEIPT */
router.post('/delivery-receipt/:requestType', async (req, res, next) => {
    let requestType = req.params.requestType
    let { tracking_number, beneficiary_name, allocations, remarks, receiver, printing_date } = req.body
    let template = ''

    if (requestType == 'P') {
        template = '/receipt_template/receipt.html'
    } else {
        template = '/receipt_template/token_receipt.html'
        allocations = '[]'
    }

    let compiled = ejs.compile(fs.readFileSync(path.join(__dirname, template), 'utf8'))
    let html = compiled({
        tracking_number: tracking_number,
        beneficiary_name: beneficiary_name,
        allocations: JSON.parse(allocations),
        remarks: remarks,
        receiver: receiver, 
        printing_date: printing_date
    })
    let options = { format: 'Letter' }

    pdf.create(html, options).toFile('./public/export.pdf', (err, result) => {
        if (err) console.log(err)

        // write file and return filepath
        console.debug(result)
        downloadFilepath = result.filename
        downloadFilename = `${tracking_number}_${beneficiary_name}_DR.pdf`
        res.send(result)
    })
})

router.get('/delivery-receipt/', async (req, res, next) => {
    res.download(downloadFilepath, downloadFilename, (err) => {
        console.log('Download finished, deleting file from server...')
        fs.unlinkSync(downloadFilepath)
        downloadFilename = ''
        downloadFilepath = ''
    })
})

module.exports = router;
