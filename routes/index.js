var express = require('express');
var router = express.Router();

const db = require('../db')

/* GET home page. */
router.get('/', async (req, res, next) => {
    if (req.session.user && req.cookies.user_sid) {
        let allTotalRequestsQry = `
            SELECT r.id request_id, r.tracking_number, b.id beneficiary_id, b.name, 
                r.allocation, r.request_type, r.requests, r.remarks, r.receiver,
                b.designation, b.type, b.contact_person, b.contact_number, b.classification,
                c.id city_id, c.name city_name, p.id province_id, p.name province_name, rg.id region_id, rg.name region_name,
                CONCAT(c.name || ', ' || p.name) AS location, r.tag, r.created_at, date(r.created_at) request_date, r.date_delivered
            FROM ${db.schema}.beneficiaries b, ${db.schema}.requests r, ${db.schema}.cities c, ${db.schema}.provinces p, ${db.schema}.regions rg
            WHERE 1=1 
            AND r.beneficiary_id = b.id
            AND b.city_id = c.id
            AND c.province_id = p.id
            AND p.region_id = rg.id
            AND r.status = 'A'
            ORDER BY r.created_at DESC`
        let allRegionsQuery = `SELECT id, name FROM ${db.schema}.regions`
        let allProvincesQuery = `SELECT id, name, region_id FROM ${db.schema}.provinces`
        let allCitiesQuery = `SELECT id, name, province_id FROM ${db.schema}.cities`

        let requestQueryResult = await db.asyncQuery(allTotalRequestsQry)
        let requests = requestQueryResult.rows

        let regionsQueryResult = await db.asyncQuery(allRegionsQuery)
        let allRegions = regionsQueryResult.rows

        let provincesQueryResult = await db.asyncQuery(allProvincesQuery)
        let allProvinces = provincesQueryResult.rows

        let citiesQueryResult = await db.asyncQuery(allCitiesQuery)
        let allCities = citiesQueryResult.rows

        res.render('index', { 
            title: 'Project HERMeS',
            username: req.session.user.username,
            role: req.session.user.role,
            first_login: req.session.user.first_login,
            allTotalRequests: requests,
            allRegions: allRegions,
            allProvinces: allProvinces,
            allCities: allCities
        })
    } else {
        res.redirect('/login');
    }
})

module.exports = router