var express = require('express')
var router  = express.Router()

router.get('/', async (req, res, next) => {
    if (req.session.user && req.cookies.user_sid) {
        res.render('users', {
            title: 'Project HERMeS - User Management',
            username: req.session.user.username,
            role: req.session.user.role,
            first_login: req.session.user.first_login,
        })
    } else {
        res.redirect('/login')
    }
})

module.exports = router