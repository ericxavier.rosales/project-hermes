var express = require('express')
var router  = express.Router()

const db            = require('../db')

router.get('/', async (req, res, next) => {
    if (req.session.user && req.cookies.user_sid) {
        if (req.session.user.role != 'RA') {
            let allRequestsQuery = `
                SELECT r.id request_id, r.tracking_number, b.id beneficiary_id, b.name, 
                    r.allocation, r.request_type, r.requests, r.remarks, r.receiver,
                    b.designation, b.type, b.contact_person, b.contact_number, b.classification,
                    c.id city_id, c.name city_name, p.id province_id, p.name province_name, rg.id region_id, rg.name region_name,
                    CONCAT(c.name || ', ' || p.name) AS location, r.tag, r.created_at, r.date_delivered
                FROM ${db.schema}.beneficiaries b, ${db.schema}.requests r, ${db.schema}.cities c, ${db.schema}.provinces p, ${db.schema}.regions rg
                WHERE 1=1 
                AND r.beneficiary_id = b.id
                AND b.city_id = c.id
                AND c.province_id = p.id
                AND p.region_id = rg.id
                AND r.status = 'A'
                AND r.tag = 0
                ORDER BY r.created_at DESC`
            let allRegionsQuery = `SELECT id, name FROM ${db.schema}.regions`
            let allProvincesQuery = `SELECT id, name, region_id FROM ${db.schema}.provinces`
            let allCitiesQuery = `SELECT id, name, province_id FROM ${db.schema}.cities`
            
            let requestQueryResult = await db.asyncQuery(allRequestsQuery)
            let newRequests = requestQueryResult.rows

            let regionsQueryResult = await db.asyncQuery(allRegionsQuery)
            let allRegions = regionsQueryResult.rows

            let provincesQueryResult = await db.asyncQuery(allProvincesQuery)
            let allProvinces = provincesQueryResult.rows

            let citiesQueryResult = await db.asyncQuery(allCitiesQuery)
            let allCities = citiesQueryResult.rows

            res.render('requests', { 
                title: 'Project HERMeS',
                header: 'Requests',
                username: req.session.user.username,
                role: req.session.user.role,
                newRequests: newRequests,
                allRegions: allRegions,
                allProvinces: allProvinces,
                allCities: allCities
            })
        } else {
            res.redirect('/')
        }
    } else {
        res.redirect('/login');
    }
})

router.patch('/:request_id', async (req, res, next) => {
    let request = req.body
    let requestId = req.params.request_id
    console.log(`Updating request with ID: ${requestId}`)
    
    let updateBeneficiaryQry = `
        UPDATE ${db.schema}.beneficiaries
		SET name = $1,
			contact_person = $2,
            contact_number = $3,
            designation = $4,
            type = $5,
            classification = $6,
            city_id = $7,
            updated_at = NOW()
        WHERE id = $8
    `
    let updateBeneficiaryValues = [
		request.Name,
        request.ContactPerson, 
        request.ContactNumber, 
        request.Designation, 
        request.Type, 
        request.Classification,
        request.CityId, 
        request.BeneficiaryId 
    ]

    let updateRequestQry = `
        UPDATE ${db.schema}.requests
        SET request_type = $1,
            receiver = $2,
            allocation = $3,
            requests = $4,
			remarks = $5,
			date_delivered = $6,
            updated_at = NOW()
        WHERE id = $7
    `

    let updateRequestValues = [ 
        request.RequestType, 
        request.Receiver, 
        request.Allocation, 
        request.Requests, 
		request.Remarks, 
		(request.DeliveryDate != '') ? request.DeliveryDate : null,
        requestId 
    ]

    let client = await db.getClient()

    try {
        console.log('Starting update queries...')
        await client.query('BEGIN')
        let updateBeneficiaryResult = await client.query(updateBeneficiaryQry, updateBeneficiaryValues)
        let updateRequestResult = await client.query(updateRequestQry, updateRequestValues)
        await client.query('COMMIT')
    } catch (e) {
        console.log('Error encountered. Rolling back transactions...')
        await client.query('ROLLBACK')
        res.send(false)
        throw e
    } finally {
        client.release()
        console.log('Queries finished. Client released.')
    }

    res.send(request)

    console.log('Update execution finished.')
})

router.put('/:request_id', async (req, res, next) => {
    let request = req.body
    let requestId = req.params.request_id
    console.log(`Updating request with ID: ${requestId}`)
    
    let confirmRequestQry1 = `
        UPDATE ${db.schema}.beneficiaries
		SET name = $1, 
			contact_person = $2,
            contact_number = $3,
            designation = $4,
            type = $5,
            classification = $6,
            city_id = $7,
            updated_at = NOW()
        WHERE id = $8
    `
    let confirmRequestValues1 = [ 
		request.Name,
        request.ContactPerson, 
        request.ContactNumber, 
        request.Designation, 
        request.Type, 
        request.Classification,
        request.CityId, 
        request.BeneficiaryId 
    ]

    let confirmRequestQry2 = `
        UPDATE ${db.schema}.requests
        SET request_type = $1,
            receiver = $2,
            allocation = $3,
            requests = $4,
            remarks = $5,
            updated_at = NOW(),
            tag = 1
        WHERE id = $6
    `

    let confirmRequestValues2 = [ 
        request.RequestType, 
        request.Receiver, 
        request.Allocation, 
        request.Requests, 
        request.Remarks, 
        requestId 
    ]

    let client = await db.getClient()

    try {
        console.log('Starting update queries...')
        await client.query('BEGIN')
        let updateBeneficiaryResult = await client.query(confirmRequestQry1, confirmRequestValues1)
        let updateRequestResult = await client.query(confirmRequestQry2, confirmRequestValues2)
        await client.query('COMMIT')
    } catch (e) {
        console.log('Error encountered. Rolling back transactions...')
        await client.query('ROLLBACK')
        res.send(false)
        throw e
    } finally {
        client.release()
        console.log('Queries finished. Client released.')
    }

    res.send(request)

    console.log('Update execution finished.')
})

router.post('/', async (req, res, next) => {
    let { name, contactPerson, contactNumber, city } = req.body

    // run insert query
    let newBeneficiaryQry = {
        text: `
            INSERT INTO ${db.schema}.beneficiaries(name, contact_person, contact_number, city_id) 
            VALUES($1, $2, $3, $4) 
            RETURNING id`,
        values: [ name, contactPerson, contactNumber, city ]
    }

    let client = await db.getClient()

    try {
        console.log('Executing insert new beneficiary query...')
        await client.query('BEGIN')

        // Insert new beneficiary first
        let newBeneficiaryResult = await db.asyncQuery(newBeneficiaryQry.text, newBeneficiaryQry.values)
        if (!newBeneficiaryResult) {
            console.debug(newBeneficiaryResult)
            console.error('Error encountered')
            res.send(false)
        }

        let id = newBeneficiaryResult.rows[0].id
        let tracking_number = 'OVPCVRD' + ('' + id).padStart(5, '0')

        console.debug(`Beneficiary ID: ${id}`)
        console.debug(`Tracking Number: ${tracking_number}`)
        let newRequestQry = {
            text: `
                INSERT INTO ${db.schema}.requests(tracking_number, beneficiary_id) 
                VALUES($1, $2) 
                RETURNING id`,
            values: [ tracking_number, id ]
        }

        console.log('Executing insert new request query...')
        let newRequestResult = await client.query(newRequestQry.text, newRequestQry.values)
        if (!newRequestResult) {
            await client.query('ROLLBACK', [])
            console.debug(newRequestResult)
            console.error('Error encountered logging new request')
            res.send(false)
        }
        await client.query('COMMIT', [])
        console.log('Request logged successfully.')
        
        // Retrieve location details
        let getLocationQry = `
            SELECT CONCAT(c.name || ', ' || p.name) AS location
            FROM ${db.schema}.cities c, ${db.schema}.provinces p
            WHERE c.province_id = p.id
            AND c.id = $1`
        let getLocationResult = await db.asyncQuery(getLocationQry, [ city ])

        let locationName = getLocationResult.rows[0].location
        let requestId    = newRequestResult.rows[0].id

        res.send({
            request_id: requestId,
            beneficiary_id: id,
            trackingNumber: tracking_number,
            name: name,
            location: locationName,
            contactPerson: contactPerson,
            contactNumber: contactNumber
        })

    } catch(e) {
        await client.query('ROLLBACK')
        res.send(false)
        throw e
    } finally {
        client.release()
        console.log('Queries finished, releasing client...')
    }

    console.log('Sending reponse...')
})

router.post('/validate', async(req, res, next) => {
    let { name, city } = req.body
    console.log('Validating for duplicate entry...')

    let validationQry = `
        SELECT b.id, r.tag
        FROM ${db.schema}.beneficiaries b, ${db.schema}.requests r
        WHERE UPPER(b.name) = UPPER($1)
        AND b.city_id = $2
        AND b.status = 'A'
        and r.beneficiary_id = b.id
        ORDER BY r.created_at DESC
    `
    let validationValues = [ name, city ]

    let validationResult = await db.asyncQuery(validationQry, validationValues)

    res.send(validationResult)
})

router.delete('/', async(req, res, next) => {
    console.log('[Delete request received]')
    console.log(typeof req.body['requestIds[]'])
    let requestIds = req.body['requestIds[]']

    let requestIdsString = (typeof requestIds == 'string') ? requestIds : requestIds.join(', ')
    let getBeneficiariesQry = `
        SELECT beneficiary_id
        FROM ${db.schema}.requests
        WHERE id IN (${requestIdsString})`

    let getBeneficiariesResult = await db.asyncQuery(getBeneficiariesQry, [])
    if (!getBeneficiariesResult) {
        console.debug(getBeneficiariesResult)
        console.error('Error encountered')
        res.send(false)
    }

    let beneficiaryIdRows = getBeneficiariesResult.rows
    let beneficiaryIds = []
    beneficiaryIdRows.forEach((row, index) => {
        beneficiaryIds.push(row.beneficiary_id)
    })
    let beneficiaryIdsString = beneficiaryIds.join(', ')

    let deleteRequestsQry = `
        UPDATE ${db.schema}.requests
        SET status = 'D',
        updated_at = NOW()
        WHERE id IN (${requestIdsString})`
    let deleteBeneficiariesQry = `
        UPDATE ${db.schema}.beneficiaries
        SET status = 'D',
        updated_at = NOW()
        WHERE id IN (${beneficiaryIdsString})`

    let client = await db.getClient()

    try {
        console.log('Executing delete queries...')
        await client.query('BEGIN')
        await client.query(deleteRequestsQry)
        await client.query(deleteBeneficiariesQry)
        await client.query('COMMIT')
    } catch(e) {
        await client.query('ROLLBACK')
        throw e 
    } finally {
        client.release()
        console.log('Delete finished, releasing client...')
    }

    console.log('Sending response to client...')
    res.send(true)
})

module.exports = router
