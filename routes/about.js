var express = require('express')
var router  = express.Router()

router.get('/', async (req, res, next) => {
    if (req.session.user && req.cookies.user_sid) {
        res.render('about', {
            title: 'Project HERMeS - About Hiveqore',
            username: req.session.user.username,
            role: req.session.user.role
        })
    } else {
        res.redirect('/login')
    }
})

module.exports = router