var createError   = require('http-errors');
var express       = require('express');
var path          = require('path');
var session       = require('express-session')
var bodyParser    = require('body-parser')
var cookieParser  = require('cookie-parser');
var logger        = require('morgan');

// Routers 
var loginRouter       = require('./routes/login')
var logoutRouter      = require('./routes/logout')
var usersRouter       = require('./routes/users')
var indexRouter       = require('./routes/index')
var aboutRouter       = require('./routes/about')
var requestsRouter    = require('./routes/requests')
var approvalRouter    = require('./routes/approval')
var fulfillmentRouter = require('./routes/fulfillment')
var dispatchRouter    = require('./routes/dispatch')
var deliveryRouter    = require('./routes/delivery')

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static(path.join(__dirname, 'node_modules')))

// user session 
app.use(session({
  key: 'user_sid',
  secret: 'ovp_hermes_secret',
  saveUninitialized: false,
  resave: true,
  rolling: true,
  cookie: {
    expires: 18000000
  }
}))

app.use((req, res, next) => {
  if (req.cookies.user_sid && !req.session.user) {
    res.clearCookie('user_sid')
  }
  next()
})

// Routes 
app.use('/',            indexRouter)
app.use('/login',       loginRouter)
app.use('/logout',      logoutRouter)
app.use('/users',       usersRouter)
app.use('/about',       aboutRouter)
app.use('/requests',    requestsRouter)
app.use('/approval',    approvalRouter)
app.use('/fulfillment', fulfillmentRouter)
app.use('/dispatch',    dispatchRouter)
app.use('/delivery',    deliveryRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

var sessionChecker = (req, res, next) => {
  if (req.session.user && req.cookies.user_sid) {
      res.redirect('/login');
  } else {
      next();
  }    
}

module.exports = app;
